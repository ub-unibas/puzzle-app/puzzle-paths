use puzzle_paths::build_jigsaw_template;

fn main() {
    let paths = build_jigsaw_template(1024.0, 746.0, 3, 3, None, None, None);
    print!("{:?}", paths);
}
