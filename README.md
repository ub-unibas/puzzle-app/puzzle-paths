# Puzzle Paths

This library creates SVG paths suitable for cutting out jigsaw puzzle pieces of
an image.

## Documentation

- See https://docs.rs/puzzle-paths for stable docs
- See https://ub-unibas.pages.switch.ch/puzzle-app/puzzle-paths/puzzle_paths for dev build docs

## Acknowledgements

The library is heavily inspired by [@Draradech](https://github.com/Draradech)'s
[Jigsaw Puzzle Generator](https://github.com/Draradech/jigsaw) (see it
[here](https://draradech.github.io/jigsaw/jigsaw.html) in action).
